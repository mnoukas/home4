import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      System.out.println(valueOf("2/233"));
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      this.numerator = a;
      if (b == 0) {
         throw new RuntimeException("Nimetaja ei saa olla 0.");
      }
      this.denominator = b;
   }

   /** Reduce this fraction (and make denominator > 0).
    * @return reduced fraction
    * Viide: enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public Lfraction taandamine () {
      Lfraction f = null;
      try {
         f = (Lfraction)clone();
      } catch (CloneNotSupportedException e) {};
      if (denominator == 0)
         throw new RuntimeException ("Vale nimetaja 0!");
      if (denominator < 0) {
         f.numerator = -numerator;
         f.denominator = -denominator;
      }
      if (numerator == 0)
         f.denominator = 1;
      else {
         long n = commonDenominator (numerator, denominator);
         f.numerator = numerator / n;
         f.denominator = denominator / n;
      }
      return f;
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return this.numerator + " / " + this.denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    * Viide: http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   @Override
   public boolean equals (Object m) {
      return (this.compareTo((Lfraction)m) == 0);
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.numerator, this.denominator);
   }

   /** Greatest common divisor of two given integers.
    * @param a first integer
    * @param b second integer
    * @return GCD(a,b)
    * Viide: enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   private long commonDenominator(long a, long b) {
      long m = Math.max (Math.abs (a), Math.abs (b));
      if (m == 0) throw new RuntimeException ("Ühine nimetaja 0!");
      long n = Math.min (Math.abs (a), Math.abs (b));
      while (n > 0) {
         a = m % n;
         m = n;
         n = a;
      }
      return m;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    * Viide: http://codereview.stackexchange.com/questions/83245/mathematical-functions-on-fractions
    * Viide: enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public Lfraction plus (Lfraction m) {
      return new Lfraction (numerator * m.denominator + denominator * m.numerator,
              denominator * m.denominator).taandamine();
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long korrutame1 = this.numerator * m.numerator;
      long korrutame2 = this.denominator * m.denominator;

      Lfraction korrutatudObjekt = new Lfraction(korrutame1, korrutame2);
      return korrutatudObjekt.taandamine();
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {

      long num = this.numerator < 0 ? -1 : 1;
      long den = this.denominator < 0 ? -1 : 1;
      if (den == 0)
         throw new RuntimeException("Nulliga jagamine ei ole võimalik!");
      Lfraction turnAround = new Lfraction(this.denominator * num * den,this.numerator * num * den);
      return turnAround;
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {

      long miinuseksMuutja = -1;
      Lfraction minusM2rk = new Lfraction(miinuseksMuutja * this.numerator, this.denominator);
      return minusM2rk;
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long temp1 = this.numerator * m.denominator;
      long temp2 = this.numerator * (temp1/this.denominator) - m.numerator * (temp1/m.denominator);

      Lfraction minusObject = new Lfraction(temp2, temp1);
      return minusObject.taandamine();
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.denominator == 0 || this.denominator == 0) {
         throw new RuntimeException("Nulliga ei saa jagada!");
      }
     return this.times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long muutuja1 = this.numerator * m.denominator;
      long muutuja2 = this.denominator * m.numerator;
      int vastus = 0;
      if (muutuja1 == muutuja2) {
         return vastus;
      } else if (muutuja1 < muutuja2) {
         return -1;
      } else {
         return 1;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      Lfraction uus = new Lfraction(this.numerator, this.denominator);
      return uus;
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (long) Math.floor(Double.valueOf(this.numerator / this.denominator));
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    * Viide: http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public Lfraction fractionPart() {
      return new Lfraction (this.numerator - this.denominator * integerPart(), this.denominator
      ).taandamine();
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      double uusDouble1 = (double) this.numerator;
      double uusDouble2 = (double) this.denominator;
      return uusDouble1 / uusDouble2;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    * http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public static Lfraction toLfraction (double f, long d) {
      if (d > 0)
         return new Lfraction ((int)(Math.round (f*d)), d);
      else
         throw new ArithmeticException (" Vale nimetaja: " + d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    * Viide: http://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
    */
   public static Lfraction valueOf (String s) {
      s = s.trim();
      if (s.length() < 3) throw new RuntimeException("Tühi või teadmata string: " + s);
      if (Character.isLetter(s.charAt(0)) || Character.isLetter(s.charAt(2)))
         throw new RuntimeException("Ärge sisestage tähti palun.");
      String[] jupid = s.split("/");
      if (jupid.length > 2) {
         throw new RuntimeException("Vale sisestatud parameeter: " + s);
      }
      Lfraction tulemusObjekt = new Lfraction(Long.parseLong(jupid[0].trim()),
              Long.parseLong(jupid[1].trim()));
      return (tulemusObjekt.taandamine());
   }
}

